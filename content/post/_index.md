---
title: "Articles"
date: 2017-03-02T12:00:00-05:00
---

Les articles sont paginés suivant le paramètre `Paginate = 3` du fichier `config.toml`.
Libre à vous de le modifier pour votre site.
