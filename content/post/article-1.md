+++
title =  "Impression 3D"
date = 2020-06-30T19:43:48+02:00
tags = ['Impression 3D', 'IMTS']
featured_image = "/images/noeud.jpg"
description = "Un noeud de Möbius"
+++

Le ruban de Möbius est cette forme topologique à une seule face. C'est possible et mystérieux. Et cela rend difficile les parties de pile ou face.

> “ Laisser glisser votre doigt sur une face de la forme et vous ne reviendrez à votre point de départ qu'une fois avoir parcourue l'ensemble de la (sur)face de la forme. ”

Autant dire que l'idée de pouvoir imprimer en 3D une telle forme me laisse admiratif. Nous l'avons donc fait au Fablab de l'Institut Solacroup et le résultat est plutôt magique.

{{< figure src="/hugo-ananke/images/noeud.jpg" title="Un ruban de Möbius imprimé en 3D" >}}
